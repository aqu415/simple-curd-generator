package ${package.Other};

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import com.xx.bean.convertor.AbstractConvertor;
import ${package.Entity}.${entity};

@Mapper
public interface ${entity}Convertor extends AbstractConvertor<${entity}VO, ${entity}DTO, ${entity}> {
    ${entity}Convertor INSTANCE = Mappers.getMapper(${entity}Convertor.class);
}
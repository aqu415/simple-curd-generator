# simple-curd-generator

#### 介绍

在开发后台管理系统的时候，感觉基本上操作都是一样的：curd，于是借助并扩展mp的代码生成器自动生成一套api,减少重复劳动而且还统一规范，何乐而不为？
目标：单表操作代码生成器，自动生成CRUD方法，带DTO、VO、Convertor等

#### 软件架构

常规架构：依赖mybatis,mybatis-plus,spring-boot,lombok,mapstruct等

#### 使用方式

只需要在 com.xx.Generator 类里修改表名和其他属性然后执行单元测试即可


> mybatis-plus 官网: https://baomidou.com/
> 官网上面的资料很全，本文以：
>  <mybatis-plus-generator.version>3.5.1</mybatis-plus-generator.version>
>  为例子，高于这个版本的貌似把之前一些比较好用的功能阉割掉了

__在开发后台管理系统的时候，感觉基本上操作都是一样的：curd，于是借助并扩展mp的代码生成器自动生成一套api，减少重复劳动而且还统一规范，何乐而不为？__

__目标：通过插件，输入表名后自动生成一套增删改查api__

#### 工程价绍
项目分为3个模块：core、business、generator，依赖关系是：generator->business->core
generator里主要是代码生成器
business是我们主要的业务代码
core是一些基础的类

![在这里插入图片描述](https://img-blog.csdnimg.cn/d47395f5f9e14d128b963c3d44396fd0.png)

#### 代码生成器入口
为了和生成的代码区分开，代码生成器被设计成了一个Junit测试类

![在这里插入图片描述](https://img-blog.csdnimg.cn/0f7a2d5f2cb34525a459c60a4d2a5fbd.png)
#### 使用方式
我们只要在Generator 类里修改jdbc配置和需要处理的表名即可，下面备注都很详细；

```
package com.xx;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.xx.lib.EnhanceFreemarkerTemplateEngine;
import org.junit.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * https://baomidou.com/pages/779a6e/ 新
 * <p>
 * https://baomidou.com/pages/d357af/ 旧（参数详解）
 * <p>
 * 自定义模板有哪些可用参数？Github (opens new window)AbstractTemplateEngine 类中方法 getObjectMap 返回 objectMap 的所有值都可用。
 */
public class Generator {

    // test 123 456
    @Test
    public void generatorTest() {

        // 定位
        String finalProjectPath = System.getProperty("user.dir");
        System.out.println(finalProjectPath);

        // 执行
        FastAutoGenerator.create("jdbc:mysql://localhost:3306/test?useUnicode=true&characterEncoding=UTF-8", "root", "root").globalConfig(builder -> {
                    builder.author("kanan") // 设置作者
                            .fileOverride().enableSwagger() // 开启 swagger 模式
                            .disableOpenDir() //禁止打开输出目录
                            .outputDir(finalProjectPath + "/src/main/java"); // 指定输出目录
                }).packageConfig(builder -> {
                    builder.parent("com.xx") // 设置父包名
                            .pathInfo(Collections.singletonMap(OutputFile.mapperXml, finalProjectPath + "/src/main/resources")); // 设置mapperXml生成路径
                }).strategyConfig(builder -> {
                    // 需要被解析的表名
                    builder.addInclude("django_admin_log");
                    builder.entityBuilder().enableLombok().enableTableFieldAnnotation();
                    builder.controllerBuilder().enableRestStyle();
                }).injectionConfig(consumer -> {
                    Map<String, String> customFile = new HashMap<>();
                    // DTO 下面的key会作为类名后缀，进而生成新类
                    customFile.put("DTO.java", "templates/other/dto.java.ftl");
                    customFile.put("VO.java", "templates/other/vo.java.ftl");
                    customFile.put("Convertor.java", "templates/other/convertor.java.ftl");
                    consumer.customFile(customFile);
                })
                // EnhanceFreemarkerTemplateEngine 里主要重写对自定义类的处理 如vo dto convert等
                .templateEngine(new EnhanceFreemarkerTemplateEngine()).execute();
    }
}

```
#### 效果
代码会生成在generator项目下，我们只需要按自己的规则把生成的代码移动到business目录即可；

![在这里插入图片描述](https://img-blog.csdnimg.cn/49e95b04596a41c1a7e927157023e982.png)

##### controller方法
目前暂时预先提供这几个方法，后续再逐步丰富

![在这里插入图片描述](https://img-blog.csdnimg.cn/98fd2ebdb869496a948ffdd9e3110faf.png)

生成样例代码如下：

```
package com.xx.controller;


import com.xx.entity.*;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import com.xx.service.*;
import com.xx.other.*;
import javax.validation.constraints.NotEmpty;
import com.xx.BizResult;
import com.xx.bean.entity.PageRequest;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author kanan
 * @since 2023-02-11
 */
@RestController
@RequestMapping("/auth-permission")
public class AuthPermissionController {

    /**
     * service
     */
    @Resource
    private IAuthPermissionService service;


    /**
     * 保存
     *
     * @param dto 参数
     * @return 保存结果
     */
    @GetMapping
    public BizResult<Boolean> saveOne(@Validated AuthPermissionDTO dto) {
        return BizResult.success(this.service.saveOne(dto));
    }

    /**
     * 根据主键查询VO
     *
     * @param pk 主键
     * @return VO
     */
    @GetMapping
    public BizResult<AuthPermissionVO> getByPk(@Validated @NotEmpty String pk) {
        return BizResult.success(this.service.getByPk(pk));
    }

    /**
     * 根据主键删除
     *
     * @param pk 主键
     * @return 删除结果
     */
    @DeleteMapping("/{pk}")
    public BizResult<Object> deleteByPk(@Validated @NotEmpty @PathVariable("pk") String pk) {
        return BizResult.success(this.service.deleteByPk(pk));
    }


    /**
     * 分页查询
     *
     * @return BizResult
     */
    @GetMapping("/page")
    public BizResult<IPage<AuthPermissionVO>> page(AuthPermissionDTO param, PageRequest request) {
        IPage<AuthPermission> page = new Page<>(request.getPageNum(), request.getPageSize());
        IPage<AuthPermissionVO> list = this.service.selectPageByDto(page, param);
        return BizResult.success(list);
    }
}

```

#### 后记

目前模板还在不断完善，如果需要自定义修改模板只需要修改generator里的模板即可；

![在这里插入图片描述](https://img-blog.csdnimg.cn/88bbeffdab8949a295c2baa1475533f7.png)


代码已上传到： https://gitee.com/aqu415/simple-curd-generator 可以  [【 下载下来】](https://gitee.com/aqu415/simple-curd-generator) 调试
over~~